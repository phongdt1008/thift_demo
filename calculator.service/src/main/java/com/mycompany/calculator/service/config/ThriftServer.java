/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.calculator.service.config;

import com.mycompany.calculator.lib.TCalculatorService;
import com.mycompany.calculator.service.controller.CalculatorController;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.server.TServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Phong
 */
@Configuration
public class ThriftServer {

    @Bean
    public TProtocolFactory tProtocolFactory() {
        return new TBinaryProtocol.Factory();
    }

    @Bean
    public ServletRegistrationBean thriftBookServlet(TProtocolFactory protocolFactory, CalculatorController controller) {
        TServlet tServlet = new TServlet(new TCalculatorService.Processor<>(controller), protocolFactory);

        return new ServletRegistrationBean(tServlet, "/calculator");
    }
}
