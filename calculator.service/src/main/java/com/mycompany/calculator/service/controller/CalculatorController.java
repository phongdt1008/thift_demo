/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.calculator.service.controller;

import com.mycompany.calculator.lib.TCalculatorService;
import com.mycompany.calculator.lib.TOperation;
import org.apache.thrift.TException;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Phong
 */
@Controller
public class CalculatorController implements TCalculatorService.Iface {

    @Override
    public int calculate(int num1, int num2, TOperation op) throws TException {
        switch (op) {
            case ADD:
                return num1 + num2;
            case DIVIDE:
                return num1 / num2;
            case MULTIPLY:
                return num1 * num2;
            case SUBTRACT:
                return num1 - num2;
            default:
                return num1 + num2;
        }
    }

}
