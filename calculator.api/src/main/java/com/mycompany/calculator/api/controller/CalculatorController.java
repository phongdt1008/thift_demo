/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.calculator.api.controller;

import com.mycompany.calculator.api.config.CalculatorClient;
import com.mycompany.calculator.lib.TOperation;
import javax.servlet.http.HttpServletRequest;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phong
 */
@RestController
public class CalculatorController {

    @Autowired
    private CalculatorClient client;

    @GetMapping(value = "/add")
    public int add(HttpServletRequest request) throws TException {
        int num1 = Integer.parseInt(request.getParameter("num1"));
        int num2 = Integer.parseInt(request.getParameter("num2"));
        return client.calculate(num1, num2, TOperation.ADD);
    }

}
